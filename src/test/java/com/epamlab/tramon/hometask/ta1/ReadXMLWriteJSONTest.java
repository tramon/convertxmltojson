package com.epamlab.tramon.hometask.ta1;


import org.jbehave.core.annotations.Given;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.io.FileReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by tramon on 04.01.2017.
 */
public class ReadXMLWriteJSONTest {

    @Given("Given an XML file where $name of person $id ")
    @Test(timeout = 1000)
    public void AsAUserIWantToParseXMLSoThatIReceiveJSONFile() throws ParseException, IOException {

        ReadXMLWriteJSON readXMLWriteJSON = new ReadXMLWriteJSON();
        JSONParser parser = new JSONParser();
        String xmlFilePath = "src/main/resources/homeTask1.xml";
        String jsonPath = "src/main/resources/homeTask1.json";
        String assertion = "Alan";
        String methodResult = "";


        readXMLWriteJSON.readFromXML(xmlFilePath);
        Object jsonFileObject = parser.parse(new FileReader(jsonPath));
        JSONObject jsonObject =  (JSONObject) jsonFileObject;
        String name = jsonObject.toJSONString();
        Object parsedJson = parser.parse(name);
        JSONObject jsonObj = (JSONObject) parsedJson;
        JSONObject rootNode = (JSONObject) jsonObj.get("root");
        JSONArray personsArray = (JSONArray)rootNode.get("person");
        JSONObject personsFirstElement = (JSONObject) personsArray.get(0);
        methodResult = (String) personsFirstElement.get("name") ;
        System.out.println("Result of reading JSON: '" + methodResult + "'");


        assertEquals(assertion, methodResult);

    }


    public void whenTest() {

    }

    public void thenTest() {

    }



}