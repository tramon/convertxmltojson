Parse specific XML file to JSON file

Narrative:
As a User
I want to parse XML file and convert it into a JSON file
So that i can use newly converted JSON file

Narrative: (new version)
In order to work with JSON file
As a User
I want to parse existing XML file and convert it into a new JSON file

Scenario: Parsing existing XML file to JSON file
Meta:
HomeTask 1.1 Test Automation.

Given an XML file
When User runs the XMLToJSONParser App
Then XML file is parsed and converted into a new JSON file

