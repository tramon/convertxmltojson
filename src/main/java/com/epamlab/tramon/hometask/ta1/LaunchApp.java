package com.epamlab.tramon.hometask.ta1;

/**
 * Launcher class
 * This Application reads information from a specific XML file (homeTask1.xml) and writes this JSON file in java/main/resources.
 * (not in target)
 */

public class LaunchApp {

    public static void main(String[] args) {
        ReadXMLWriteJSON readXMLWriteJSON = new ReadXMLWriteJSON();

        String xmlFilePath = "src/main/resources/homeTask1.xml";
        System.out.println(readXMLWriteJSON.readFromXML(xmlFilePath));
    }

}





