package com.epamlab.tramon.hometask.ta1;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class ReadXMLWriteJSON {

    private List<String> names;
    private List<String> urls;
    private JSONObject root;

    public JSONObject readFromXML(String xmlFilePath) {
        String nameString = "name";
        String urlString = "url";
        String jsonPath = "src/main/resources/homeTask1.json";

        try {
            File xmlFile = new File(xmlFilePath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFile);
            document.getDocumentElement().normalize();

            names = getTextContentByNameOfTheNode(document, nameString);
            urls = getTextContentByNameOfTheNode(document, urlString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            root = new JSONObject();
            JSONObject person = new JSONObject();

            JSONObject firstPerson = new JSONObject();
            firstPerson.put(nameString, names.get(0));
            firstPerson.put(urlString, urls.get(0));

            JSONObject secondPerson = new JSONObject();
            secondPerson.put(nameString, names.get(1));
            secondPerson.put(urlString, urls.get(1));

            JSONArray persons = new JSONArray();
            persons.add(firstPerson);
            persons.add(secondPerson);

            person.put("person", persons);
            root.put("root", person);


            FileWriter file = new FileWriter(jsonPath);
            file.write(root.toJSONString());
            file.flush();
            file.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return root;
        }

    }

    private List<String> getTextContentByNameOfTheNode(Document document, String nameOfTheNode) {
        List<String> resultList = new ArrayList<String>();

        NodeList nodeList = document.getElementsByTagName(nameOfTheNode);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                resultList.add(node.getTextContent());
            }
        }
        return resultList;
    }


}
